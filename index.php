<?php


//archivo de configuracion
require './config.php';


//condicional avanzado
//var = () ? verdadero:falso;
$url = (isset($_GET["url"]) ) ? $_GET["url"] : "Index/index";
$url = explode("/", $url);


$controller = ( isset($url[0] ) ? $url[0]."_controller" : "Index_controller");
$method = ( isset($url[1]) && $url[1] != null ) ? $url[1]: "index";
$params = ( isset($url[2]) && $url[2] != null) ? $url[2]: null;

//cargador peresozo 
//funcion anonima, recibe una clase
//spl_autoloadregister  hace muchas cosas, ejecuta la clase de manera automatica

spl_autoload_register(function($class) {
    if (file_exists(LIBS . $class . ".php")) {
        require LIBS . $class . ".php";
    } elseif (MODELS . $class . ".php") {
        require MODELS . $class . ".php";
    } else {
        if (file_exists(BS . $class . ".php")) {
            require BS . $class . ".php";
        }else{
            exit("La clase " . $class . " ha sido definida");
        }
    }
});

//echo "Controller: ".$controller;
//echo "</br> Metodo: ".$method;
//echo "</br> Parametro: ".$params;

$path = "./controllers/". $controller .".php";

if (file_exists($path)) {
    //cargar con require
    require $path;
    //instancia
    $controller = new $controller();

    
        if (method_exists($controller, $method)) {
            if ( $params != null) {
                $controller->{$method}($params);
            } else {
                $controller->{$method}();
            }
        } else {
            exit("Invalid Method");
        }
    
} else {
    exit("Invalid Controllers");
}